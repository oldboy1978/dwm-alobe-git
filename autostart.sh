#!/bin/bash

# Applications

transmission-daemon &															# start transmission
dwmblocks &																		# start dwmblocks
/usr/bin/ls $HOME/Pictures/background.jpg | xargs -ro xwallpaper --stretch &	# set wallpaper
picom --config ~/.config/picom.conf &											# start picom
dunst &																			# start dunst
lxpolkit &
