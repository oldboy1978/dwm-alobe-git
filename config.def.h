/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx		= 2;        /* border pixel of windows */
static const unsigned int gappx     	= 3;        /* gaps between windows */
static const unsigned int snap      	= 32;       /* snap pixel */
static const int showbar            	= 1;        /* 0 means no bar */
static const int topbar             	= 1;        /* 0 means bottom bar */
static const int user_bh            	= 0;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          	= { "UbuntuMono Nerd Font:size=12", "JoyPixels:pixelsize=13:antialias=true:autohint=true" };
static const char dmenufont[]       	= "UbuntuMono Nerd Font:style=Regular:size=14";
static const char col1[]				= "#1b2332";
static const char col2[]				= "#c0c5ce";
static const char col3[]				= "#03e0d9";
static const char col4[]                = "#f892e5";
static const char *colors[][3]			= {
	/*               fg     bg      border   */
	[SchemeNorm] = { col2,	col1,	col1 },
	[SchemeSel]  = { col4,	col1,   col4  },
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {"st", "-n", "spterm",	NULL };
const char *spcmd2[] = {"st", "-n", "spfm",		"-e",	"ranger",		NULL };
const char *spcmd3[] = {"st", "-n",	"sphtop",	"-e",	"htop",			NULL };
const char *spcmd4[] = {"st", "-n",	"spupd",	"-e",	"arch_sync",	NULL };
const char *spcmd5[] = {"st", "-n",	"spcmus",	"-e",	"cmus",	        NULL };
const char *spcmd6[] = {"st", "-n",	"spovpn",	"-e",	"ovpn",	        NULL };
const char *spcmd7[] = {"st", "-n",	"spoint",	"-e",	"nmtui",        NULL };
static Sp scratchpads[] = {
	/* name         cmd  */
	{"spterm",		spcmd1},
	{"spranger",    spcmd2},
	{"sphtop",		spcmd3},
	{"spupd",	   	spcmd4},
	{"spcmus",		spcmd5},
	{"spovpn",		spcmd6},
	{"spoint",		spcmd7},
};

/* tagging */
static const char *tags[] = { "1.", "2.", "3.", "4.", "5.", "6.", "7.", "8." };
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class			instance    title       tags mask   isfloating   monitor    float x,y,w,h       floatborderpx*/
	{ "firefox",			NULL,       NULL,       1 << 1,		0,			-1,			50,50,1520,980,		0 },
	{ "qutebrowser",		NULL,       NULL,       1 << 1,		0,			-1,			50,50,1520,980,		0 },
	{ "Atom",				NULL,       NULL,       1 << 7,		0,			-1,			50,50,1520,980,		0 },
	{ "Virt-manager",		NULL,       NULL,       1 << 6,		0,			-1,			50,50,1520,980,		0 },
	{ "Signal",				NULL,       NULL,       1 << 7,		1,			-1,			300,100,1280,850,	0 },
	{ "ViberPC",				NULL,       NULL,       1 << 7,		1,			-1,			300,100,1280,850,	0 },
	{ NULL,					"spterm",	NULL,		SPTAG(0),	1,			-1,			250,250,1120,680,	1 },
	{ NULL,					"spfm",		NULL,		SPTAG(1),	1,			-1,			250,250,1120,680,	1 },
	{ NULL,					"sphtop",	NULL,		SPTAG(2),	1,			-1,			250,250,1120,680,	1 },
	{ NULL,					"spupd",	NULL,		SPTAG(3),	1,			-1,			250,250,1120,680,	1 },
	{ NULL,					"spcmus",	NULL,		SPTAG(4),	1,			-1,			250,250,1120,680,	1 },
	{ NULL,					"spovpn",	NULL,		SPTAG(5),	1,			-1,			250,250,1120,680,	1 },
	{ NULL,					"spoint",	NULL,		SPTAG(6),	1,			-1,			250,250,620,680,	1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-i" , NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *webcmd[]   = { "browserctl", NULL };
static const char *atomcmd[]   = { "atom", NULL };

#include <X11/XF86keysym.h>

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ControlMask,			XK_comma,  cyclelayout,    {.i = -1 } },
	{ MODKEY|ControlMask,           XK_period, cyclelayout,    {.i = +1 } },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_Left,   viewtoleft,     {0} },
	{ MODKEY,                       XK_Right,  viewtoright,    {0} },
	{ MODKEY|ShiftMask,             XK_Left,   tagtoleft,      {0} },
	{ MODKEY|ShiftMask,             XK_Right,  tagtoright,     {0} },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	{ MODKEY|ControlMask,           XK_t,      togglescratch,  {.ui = 0 } },
	{ MODKEY|ControlMask,           XK_f,      togglescratch,  {.ui = 1 } },
	{ MODKEY|ControlMask,           XK_h,      togglescratch,  {.ui = 2 } },
	{ MODKEY|ControlMask,           XK_u,      togglescratch,  {.ui = 3 } },
	{ MODKEY|ControlMask,           XK_m,      togglescratch,  {.ui = 4 } },
	{ MODKEY|ControlMask,           XK_o,      togglescratch,  {.ui = 5 } },
	{ MODKEY|ControlMask,           XK_i,      togglescratch,  {.ui = 6 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },

	/*Volume keys*/
	/*{ 0,	XF86XK_AudioMute, spawn, SHCMD("amixer set Master toggle; pkill -RTMIN+10 dwmblocks") },
	{ 0,	XF86XK_AudioLowerVolume, spawn, SHCMD("amixer set Master 5%-; pkill -RTMIN+10 dwmblocks") },
	{ 0,	XF86XK_AudioRaiseVolume, spawn, SHCMD("amixer set Master 5%+; pkill -RTMIN+10 dwmblocks") },*/
	{ 0,	XF86XK_AudioMute, spawn, SHCMD("pamixer -t; pkill -RTMIN+10 dwmblocks") },
	{ 0,	XF86XK_AudioLowerVolume, spawn, SHCMD("pamixer -d 10; pkill -RTMIN+10 dwmblocks") },
	{ 0,	XF86XK_AudioRaiseVolume, spawn, SHCMD("pamixer -i 10; pkill -RTMIN+10 dwmblocks") },

	/*Monitor brightness keys*/
	{ 0,	XF86XK_MonBrightnessUp, spawn, SHCMD("xbacklight -inc 10") },
	{ 0,	XF86XK_MonBrightnessDown, spawn, SHCMD("xbacklight -dec 10") },

	/* dunst*/
	{ControlMask,				XK_grave,	spawn,	  SHCMD("dunstctl history-pop") },
	{ControlMask,				XK_space,	spawn,	  SHCMD("dunstctl close") },

	/* Custom keys */
	{ MODKEY|ControlMask,		XK_p,   spawn,    SHCMD("powopt") },
	{ MODKEY|ControlMask,		XK_k,   spawn,    SHCMD("kbctl") },
	{ MODKEY|ControlMask,		XK_w,		spawn,		{.v = webcmd } },
	{ MODKEY|ControlMask,		XK_a,		spawn,		{.v = atomcmd } },
	/*{ MODKEY|ControlMask,		XK_n,		spawn,		SHCMD("st -e newsboat") },*/
	{ MODKEY|ControlMask,		XK_g,		spawn,		SHCMD("st -e neomutt") },
	{ MODKEY|ControlMask,		XK_d,		spawn,		SHCMD("st -e tremc") },
	{ MODKEY|ControlMask,		XK_v,		spawn,		SHCMD("virt-manager") },
	/*{ MODKEY|ControlMask,		XK_s,		spawn,		SHCMD("signal-desktop") },*/
	{ MODKEY|ControlMask,		XK_b,		spawn,		SHCMD("viber") },
	{ Mod1Mask,				XK_l,		spawn,		SHCMD("sxlock") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button1,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
